# Galician translation for authenticator.
# Copyright (C) 2021 authenticator's COPYRIGHT HOLDER
# This file is distributed under the same license as the authenticator package.
# Fran Dieguez <frandieguez@gnome.org>, 2021.
# Fran Diéguez <frandieguez@gnome.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: authenticator master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/Authenticator/issues\n"
"POT-Creation-Date: 2023-08-28 22:49+0000\n"
"PO-Revision-Date: 2023-08-30 00:10+0200\n"
"Last-Translator: Fran Diéguez <frandieguez@gnome.org>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.3.2\n"
"X-DL-Team: gl\n"
"X-DL-Module: authenticator\n"
"X-DL-Branch: master\n"
"X-DL-Domain: po\n"
"X-DL-State: None\n"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:3
#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:7
#: data/resources/ui/window.ui:36 src/application.rs:86 src/main.rs:41
msgid "Authenticator"
msgstr "Autenticador"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:4
msgid "Two-Factor Authentication"
msgstr "Autenticación de dobre factor"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:5
#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:8
#: src/application.rs:89
msgid "Generate Two-Factor Codes"
msgstr "Xere códigos de dobre factor"

#: data/com.belmoussaoui.Authenticator.desktop.in.in:10
msgid "Gnome;GTK;Verification;2FA;Authentication;OTP;TOTP;"
msgstr "Gnome;GTK;Verificación;2FA;Autenticación;OTP;TOTP;"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:6
#: data/com.belmoussaoui.Authenticator.gschema.xml.in:7
msgid "Default window width"
msgstr "Anchura da pantalla por omisión"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:11
#: data/com.belmoussaoui.Authenticator.gschema.xml.in:12
msgid "Default window height"
msgstr "Altura da pantalla por omisión"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:16
msgid "Default window maximized behaviour"
msgstr "Comportamento de xanela maximizada por omisión"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:21
msgid "Auto lock"
msgstr "Autobloquear"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:22
msgid "Whether to auto lock the application or not"
msgstr "Indicase autobloquear ou non a aplicación"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:26
msgid "Auto lock timeout"
msgstr "Tempo de espera máximo do autobloqueo"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:27
msgid "Lock the application on idle after X minutes"
msgstr "Bloquear a aplicación cando estea ocioso despois de X minutos"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:34
msgid "Download Favicons"
msgstr "Descargar Favicons"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:35
msgid ""
"Whether the application should attempt to find an icon for the providers."
msgstr ""
"Indica se a aplicación debería tentar atopar unha icona para os fornecedores."

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:39
msgid "Download Favicons over metered connections"
msgstr "Descargar Favicons en conexións medidas"

#: data/com.belmoussaoui.Authenticator.gschema.xml.in:40
msgid ""
"Whether the application should download favicons over a metered connection."
msgstr "Indica se a aplicación debería descargar os favicons en redes medidas."

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:10
msgid "Simple application for generating Two-Factor Authentication Codes."
msgstr ""
"Unha aplicación sinxela para xerar Códigos de autenticación de Doble factor."

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:11
msgid "Features:"
msgstr "Características:"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:13
msgid "Time-based/Counter-based/Steam methods support"
msgstr "Compatibilidade dos métodos Baseado-en-tempo/Baseado-en-contador/Steam"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:14
msgid "SHA-1/SHA-256/SHA-512 algorithms support"
msgstr "Compatibilidade cos algoritmos SHA-1/SHA-256/SHA-512"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:15
msgid "QR code scanner using a camera or from a screenshot"
msgstr ""
"Escáner de código QR usando unha cámara ou desde unha captura de pantalla"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:16
msgid "Lock the application with a password"
msgstr "Bloquear a aplicación con unha contrasinal"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:17
msgid "Beautiful UI"
msgstr "UI bonita"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:18
msgid "GNOME Shell search provider"
msgstr "Fornecedor de busca de GNOME Shell"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:19
msgid ""
"Backup/Restore from/into known applications like FreeOTP+, Aegis "
"(encrypted / plain-text), andOTP, Google Authenticator"
msgstr ""
"Respaldar/Restaurar desde/cara aplicacións coñecidas como FreeOTP+, Aegis "
"(cifrado / texto plano) andOTP, Google Authenticator"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:25
msgid "Main Window"
msgstr "Xanela principal"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:29
#: data/resources/ui/account_add.ui:32 data/resources/ui/account_add.ui:49
msgid "Add a New Account"
msgstr "Engadir unha nova conta"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:33
msgid "Add a New Provider"
msgstr "Engadir un novo fornecedor"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:37
msgid "Account Details"
msgstr "Detalles da conta"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:41
msgid "Backup/Restore formats support"
msgstr "Compatibilidade de formatos de Respaldo/Restauración"

#: data/com.belmoussaoui.Authenticator.metainfo.xml.in.in:296
msgid "Bilal Elmoussaoui"
msgstr "Bilal Elmoussaoui"

#: data/resources/ui/account_add.ui:6 src/widgets/preferences/window.rs:297
msgid "_Camera"
msgstr "_Cámara"

#: data/resources/ui/account_add.ui:10 src/widgets/preferences/window.rs:301
msgid "_Screenshot"
msgstr "_Captura de pantalla"

#: data/resources/ui/account_add.ui:14 src/widgets/preferences/window.rs:306
msgid "_QR Code Image"
msgstr "Imaxe de código _QR"

#: data/resources/ui/account_add.ui:58
msgid "Cancel"
msgstr "Cancelar"

#: data/resources/ui/account_add.ui:64
msgid "_Add"
msgstr "_Engadir"

#: data/resources/ui/account_add.ui:76 src/widgets/preferences/window.rs:292
msgid "Scan QR Code"
msgstr "Analizar código QR"

#: data/resources/ui/account_add.ui:114
#: data/resources/ui/account_details_page.ui:95
#: data/resources/ui/providers_dialog.ui:105
msgid "Provider"
msgstr "Fornecer"

#: data/resources/ui/account_add.ui:115
msgid "Token issuer"
msgstr "Emisor do token"

#: data/resources/ui/account_add.ui:129
#: data/resources/ui/account_details_page.ui:90
msgid "Account"
msgstr "Conta"

#: data/resources/ui/account_add.ui:135
msgid "Token"
msgstr "Token"

#: data/resources/ui/account_add.ui:141
#: data/resources/ui/account_details_page.ui:138
#: data/resources/ui/provider_page.ui:157
msgid "Counter"
msgstr "Contador"

#: data/resources/ui/account_add.ui:159
#: data/resources/ui/account_details_page.ui:118
#: data/resources/ui/provider_page.ui:132
msgid "Algorithm"
msgstr "Algoritmo"

#: data/resources/ui/account_add.ui:171
#: data/resources/ui/account_details_page.ui:128
#: data/resources/ui/provider_page.ui:117
msgid "Computing Method"
msgstr "Método de cómputo"

#: data/resources/ui/account_add.ui:183
#: data/resources/ui/account_details_page.ui:147
#: data/resources/ui/provider_page.ui:148
msgid "Period"
msgstr "Período"

#: data/resources/ui/account_add.ui:184 data/resources/ui/provider_page.ui:149
msgid "Duration in seconds until the next password update"
msgstr "Duración en segundos ate a seguinte actualización de contrasinal"

#: data/resources/ui/account_add.ui:196
#: data/resources/ui/account_details_page.ui:157
#: data/resources/ui/provider_page.ui:166
msgid "Digits"
msgstr "Díxitos"

#: data/resources/ui/account_add.ui:197 data/resources/ui/provider_page.ui:167
msgid "Length of the generated code"
msgstr "Lonxitude do código xerado"

#: data/resources/ui/account_add.ui:209
#: data/resources/ui/account_details_page.ui:167
#: data/resources/ui/provider_page.ui:111
msgid "Website"
msgstr "Sitio web"

#: data/resources/ui/account_add.ui:214
msgid "How to Set Up"
msgstr "Cómo configurar"

#: data/resources/ui/account_add.ui:234
#: data/resources/ui/preferences_camera_page.ui:4
msgid "Camera"
msgstr "Cámara"

#: data/resources/ui/account_add.ui:247
msgid "Create Provider"
msgstr "Crear fornecedor"

#: data/resources/ui/account_details_page.ui:55
#: data/resources/ui/preferences_password_page.ui:17
#: data/resources/ui/provider_page.ui:28
msgid "_Save"
msgstr "_Gardar"

#: data/resources/ui/account_details_page.ui:66
#: data/resources/ui/provider_page.ui:184
msgid "_Delete"
msgstr "_Eliminar"

#: data/resources/ui/account_details_page.ui:172
msgid "Help"
msgstr "Axuda"

#: data/resources/ui/account_row.ui:23
msgid "Increment the counter"
msgstr "Incrementar o contador"

#: data/resources/ui/account_row.ui:34
msgid "Copy PIN to clipboard"
msgstr "Copiar PIN ao portarretallos"

#: data/resources/ui/camera.ui:36
msgid "No Camera Found"
msgstr "Non se atopou unha cámara"

#: data/resources/ui/camera.ui:39
msgid "_From a Screenshot"
msgstr "_Desde unha captura de pantalla"

#: data/resources/ui/preferences_password_page.ui:4
msgid "Create Password"
msgstr "Crear contrasinal"

#: data/resources/ui/preferences_password_page.ui:28
msgid "Set up a Passphrase"
msgstr "Configurar frase de paso"

#: data/resources/ui/preferences_password_page.ui:29
msgid "Authenticator will start locked after a passphrase is set."
msgstr ""
"O autenticador iniciarase bloqueado despois de que configure unha frase de "
"paso."

#: data/resources/ui/preferences_password_page.ui:50
msgid "Current Passphrase"
msgstr "Frase de paso actual"

#: data/resources/ui/preferences_password_page.ui:56
msgid "New Passphrase"
msgstr "Nova frase de paso"

#: data/resources/ui/preferences_password_page.ui:62
msgid "Repeat Passphrase"
msgstr "Repetir frase de paso"

#: data/resources/ui/preferences_password_page.ui:76
#: data/resources/ui/provider_page.ui:72
msgid "_Reset"
msgstr "_Reiniciar"

#: data/resources/ui/preferences.ui:16
msgid "General"
msgstr "Xeral"

#: data/resources/ui/preferences.ui:19
msgid "Privacy"
msgstr "Privacidade"

#: data/resources/ui/preferences.ui:22
msgid "_Passphrase"
msgstr "_Frase de paso"

#: data/resources/ui/preferences.ui:24
msgid "Set up a passphrase to lock the application with"
msgstr "Configurar unha frase de paso coa que bloquear a aplicación"

#: data/resources/ui/preferences.ui:36
msgid "_Auto Lock the Application"
msgstr "_Autobloquear a aplicación"

#: data/resources/ui/preferences.ui:38
msgid "Whether to automatically lock the application"
msgstr "Indica se bloquear automaticamente a aplicación"

#: data/resources/ui/preferences.ui:44
msgid "Auto Lock _Timeout"
msgstr "_Tempo de espera de autobloqueo"

#: data/resources/ui/preferences.ui:45
msgid "The time in minutes"
msgstr "A hora en minutos"

#: data/resources/ui/preferences.ui:58
msgid "Network"
msgstr "Rede"

#: data/resources/ui/preferences.ui:61
msgid "_Download Favicons"
msgstr "_Descargar Favicons"

#: data/resources/ui/preferences.ui:63
msgid "Automatically attempt fetching a website icon"
msgstr "Tentar a descarga da icona do sitio automaticamente"

#: data/resources/ui/preferences.ui:68
msgid "_Metered Connection"
msgstr "Conexión _medida"

#: data/resources/ui/preferences.ui:70
msgid "Fetch a website icon on a metered connection"
msgstr "Obtén unha icona do sitio web nunha conexión medida"

#: data/resources/ui/preferences.ui:80
msgid "Backup/Restore"
msgstr "Respaldar/Restaurar"

#: data/resources/ui/preferences.ui:83 src/widgets/preferences/window.rs:481
msgid "Backup"
msgstr "Respaldar"

#: data/resources/ui/preferences.ui:88 src/widgets/preferences/window.rs:489
msgid "Restore"
msgstr "Restaurar"

#: data/resources/ui/provider_page.ui:82
msgid "Select a _File"
msgstr "Seleccione un _ficheiro"

#: data/resources/ui/provider_page.ui:105
msgid "Name"
msgstr "Nome"

#: data/resources/ui/provider_page.ui:158
msgid "The by default value for counter-based computing method"
msgstr "O valor por omisión para o método de computación baseado en contador"

#: data/resources/ui/provider_page.ui:176
msgid "Help URL"
msgstr "URL de axuda"

#: data/resources/ui/providers_dialog.ui:20
msgid "Providers"
msgstr "Fornecedores"

#: data/resources/ui/providers_dialog.ui:30 src/widgets/providers/page.rs:209
msgid "New Provider"
msgstr "Novo forncedor"

#: data/resources/ui/providers_dialog.ui:37 data/resources/ui/window.ui:218
msgid "Search"
msgstr "Buscar"

#: data/resources/ui/providers_dialog.ui:90
#: data/resources/ui/providers_list.ui:38
msgid "No Results"
msgstr "Non hai resultados"

#: data/resources/ui/providers_dialog.ui:91
msgid "No providers matching the query were found."
msgstr "Non se atoparon fornecedores que coincidan coa consulta."

#: data/resources/ui/providers_dialog.ui:136
msgid "No Provider Selected"
msgstr "Non hai fornecedor seleccionado"

#: data/resources/ui/providers_dialog.ui:137
msgid "Select a provider or create a new one"
msgstr "Seleccione un fornecedor ou cree un novo"

#: data/resources/ui/providers_list.ui:39
msgid "No accounts or providers matching the query were found."
msgstr "Non hai contas ou fornecedores que coincidan coa consulta."

#: data/resources/ui/shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Xeral"

#: data/resources/ui/shortcuts.ui:14
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Atallos de teclado"

#: data/resources/ui/shortcuts.ui:20
msgctxt "shortcut window"
msgid "Lock"
msgstr "Bloquear"

#: data/resources/ui/shortcuts.ui:26
msgctxt "shortcut window"
msgid "Preferences"
msgstr "Preferencias"

#: data/resources/ui/shortcuts.ui:32
msgctxt "shortcut window"
msgid "Quit"
msgstr "Saír"

#: data/resources/ui/shortcuts.ui:40
msgctxt "shortcut window"
msgid "Accounts"
msgstr "Contas"

#: data/resources/ui/shortcuts.ui:43
msgctxt "shortcut window"
msgid "New Account"
msgstr "Nova conta"

#: data/resources/ui/shortcuts.ui:49
msgctxt "shortcut window"
msgid "Search"
msgstr "Buscar"

#: data/resources/ui/shortcuts.ui:55
msgctxt "shortcut window"
msgid "Show Providers List"
msgstr "Mostrar a lista de fornecedores"

#: data/resources/ui/window.ui:6
msgid "_Lock the Application"
msgstr "_Bloquear a aplicación"

#: data/resources/ui/window.ui:12
msgid "P_roviders"
msgstr "_Fornecedores"

#: data/resources/ui/window.ui:16
msgid "_Preferences"
msgstr "_Preferencias"

#: data/resources/ui/window.ui:22
msgid "_Keyboard Shortcuts"
msgstr "_Atallos de teclado"

#: data/resources/ui/window.ui:26
msgid "_About Authenticator"
msgstr "_Sobre Autenticador"

#: data/resources/ui/window.ui:66
msgid "Authenticator is Locked"
msgstr "Autenticador está bloqueado"

#: data/resources/ui/window.ui:89
msgid "_Unlock"
msgstr "_Desbloquear"

#: data/resources/ui/window.ui:119
msgid "Accounts"
msgstr "Contas"

#: data/resources/ui/window.ui:134 data/resources/ui/window.ui:170
msgid "New Account"
msgstr "Nova conta"

#: data/resources/ui/window.ui:142 data/resources/ui/window.ui:212
msgid "Main Menu"
msgstr "Menú principal"

#: data/resources/ui/window.ui:150
msgid "No Accounts"
msgstr "Non hai contas"

#: data/resources/ui/window.ui:151
msgid "Add an account or scan a QR code first."
msgstr "Engadir unha conta ou analizar un código QR primeiro."

#: src/application.rs:72
msgid "Accounts restored successfully"
msgstr "Contas restauradas con éxito"

#: src/application.rs:98
msgid "translator-credits"
msgstr "Fran Diéguez <frandieguez@gnome.org>, 2021-2023"

#: src/application.rs:362 src/widgets/accounts/row.rs:46
msgid "One-Time password copied"
msgstr "Contrasinal dun só uso copiada"

#: src/application.rs:363
msgid "Password was copied successfully"
msgstr "Contrasinal copiada con éxito"

#. Translators: This is for making a backup for the aegis Android app.
#. Translators: This is for restoring a backup from the aegis Android app.
#: src/backup/aegis.rs:399 src/backup/aegis.rs:439
msgid "Aegis"
msgstr "Aegis"

#: src/backup/aegis.rs:403
msgid "Into a JSON file containing plain-text or encrypted fields"
msgstr "Nun ficheiro JSON que contén texto plano ou campos cifrados"

#: src/backup/aegis.rs:443
msgid "From a JSON file containing plain-text or encrypted fields"
msgstr "Desde un ficheiro JSON que contén texto plano ou campos cifrados"

#. Translators: This is for making a backup for the andOTP Android app.
#: src/backup/andotp.rs:79
msgid "a_ndOTP"
msgstr "a_ndOTP"

#: src/backup/andotp.rs:83
msgid "Into a plain-text JSON file"
msgstr "Nun ficheiro JSON de texto plano"

#. Translators: This is for restoring a backup from the andOTP Android app.
#: src/backup/andotp.rs:127
msgid "an_dOTP"
msgstr "an_dOTP"

#: src/backup/andotp.rs:131 src/backup/bitwarden.rs:124 src/backup/legacy.rs:45
msgid "From a plain-text JSON file"
msgstr "Desde un ficheiro JSON de texto plano"

#: src/backup/bitwarden.rs:47
msgid "Unknown account"
msgstr "Conta descoñecida"

#: src/backup/bitwarden.rs:55
msgid "Unknown issuer"
msgstr "Emisor descoñecido"

#. Translators: This is for restoring a backup from Bitwarden.
#: src/backup/bitwarden.rs:120
msgid "_Bitwarden"
msgstr "_Bitwarden"

#: src/backup/freeotp.rs:18
msgid "_Authenticator"
msgstr "_Autenticador"

#: src/backup/freeotp.rs:22
msgid "Into a plain-text file, compatible with FreeOTP+"
msgstr "Nun ficheiro de texto plano, compatíbel con FreeOTP+"

#: src/backup/freeotp.rs:51
msgid "A_uthenticator"
msgstr "A_utenticador"

#: src/backup/freeotp.rs:55
msgid "From a plain-text file, compatible with FreeOTP+"
msgstr "Desde un ficheiro de texto plano, compatíbel con FreeOTP+"

#: src/backup/freeotp_json.rs:87
msgid "FreeOTP+"
msgstr "FreeOTP+"

#: src/backup/freeotp_json.rs:91
msgid "From a plain-text JSON file, compatible with FreeOTP+"
msgstr "Desde un ficheiro JSON de texto plano, compatíbel con FreeOTP+"

#: src/backup/google.rs:19
msgid "Google Authenticator"
msgstr "Google Authenticator"

#: src/backup/google.rs:23
msgid "From a QR code generated by Google Authenticator"
msgstr "Desde un código QR xerado por Google Authenticator"

#. Translators: this is for restoring a backup from the old Authenticator
#. release
#: src/backup/legacy.rs:41
msgid "Au_thenticator (Legacy)"
msgstr "Au_tenticador (antigo)"

#: src/models/algorithm.rs:60
msgid "Counter-based"
msgstr "Baseado en contador"

#: src/models/algorithm.rs:61
msgid "Time-based"
msgstr "Baseado en tempo"

#. Translators: Steam refers to the gaming application by Valve.
#: src/models/algorithm.rs:63
msgid "Steam"
msgstr "Steam"

#: src/models/algorithm.rs:126
msgid "SHA-1"
msgstr "SHA-1"

#: src/models/algorithm.rs:127
msgid "SHA-256"
msgstr "SHA-256"

#: src/models/algorithm.rs:128
msgid "SHA-512"
msgstr "SHA-512"

#: src/widgets/accounts/add.rs:274 src/widgets/preferences/window.rs:419
#: src/widgets/providers/page.rs:292
msgid "Image"
msgstr "Imaxe"

#: src/widgets/accounts/add.rs:282 src/widgets/preferences/window.rs:427
msgid "Select QR Code"
msgstr "Seleccione un código QR"

#: src/widgets/accounts/add.rs:300
msgid "Invalid Token"
msgstr "Token non válido"

#: src/widgets/window.rs:115
msgid "Wrong Password"
msgstr "Contrasinal incorrecto"

#: src/widgets/preferences/password_page.rs:172
#: src/widgets/preferences/password_page.rs:214
msgid "Wrong Passphrase"
msgstr "Contrasinal de frase de paso"

#: src/widgets/preferences/window.rs:196 src/widgets/preferences/window.rs:268
msgid "Key / Passphrase"
msgstr "Chave / Frase de paso"

#: src/widgets/preferences/window.rs:207 src/widgets/preferences/window.rs:280
msgid "Select File"
msgstr "Seleccione un ficheiro"

#: src/widgets/preferences/window.rs:231
msgid "Failed to create a backup"
msgstr "Produciuse un fallo ao crear un respaldo"

#: src/widgets/preferences/window.rs:343
msgid "Failed to restore from camera"
msgstr "Produciuse un fallo ao restaurar desde a cámara"

#: src/widgets/preferences/window.rs:354
msgid "Failed to restore from a screenshot"
msgstr "Produciuse un fallo ao restaurar desde unha captura de pantalla"

#: src/widgets/preferences/window.rs:364
msgid "Failed to restore from an image"
msgstr "Produciuse un fallo ao restaurar desde unha imaxe"

#: src/widgets/preferences/window.rs:377
msgid "Failed to restore from a file"
msgstr "Produciuse un fallo ao restaurar desde un ficheiro"

#: src/widgets/providers/dialog.rs:235
msgid "Provider created successfully"
msgstr "Fornecedor creado con éxito"

#: src/widgets/providers/dialog.rs:245
msgid "Provider updated successfully"
msgstr "Fornecedor actualizado con éxito"

#: src/widgets/providers/dialog.rs:261
msgid "Provider removed successfully"
msgstr "Fornecedor eliminado con éxito"

#: src/widgets/providers/page.rs:186
msgid "Editing Provider: {}"
msgstr "Fornecedor de edición: {}"

#: src/widgets/providers/page.rs:325
msgid "The provider has accounts assigned to it, please remove them first"
msgstr "O fornecedor ten contas asignadas a el, elimíneas antes"

#~| msgid "Dark Theme"
#~ msgid "Dark Mode"
#~ msgstr "Tema escuro"

#~ msgid "Go Back"
#~ msgstr "Ir atrás"

#~ msgid "D_etails"
#~ msgstr "D_etalles"

#~ msgid "_Rename"
#~ msgstr "_Renomear"

#~ msgid "Appearance"
#~ msgstr "Aparencia"

#~| msgid "_Dark Theme"
#~ msgid "_Dark Mode"
#~ msgstr "Tema _escuro"

#~| msgid "Whether the application should use a dark theme"
#~ msgid "Whether the application should use a dark mode"
#~ msgstr "Indica se a aplicación debería usar o tema escuro"

#~ msgid "Used to fetch the provider icon"
#~ msgstr "Usado para obter a icona do fornecedor"

#~ msgid "Provider setup instructions"
#~ msgstr "Instrucións de configuración do fornecedor"

#~ msgid "The key that will be used to decrypt the vault"
#~ msgstr "A chave que se usará para descifrar o cofre"

#~ msgid "The key used to encrypt the vault"
#~ msgstr "A chave usada para cifrar o cofre"

#~ msgid "Select"
#~ msgstr "Seleccionar"

#~ msgid "More"
#~ msgstr "Máis"
